#include "Ini.h"
#include <cstdio>
#include <iostream>

using namespace ini;
using namespace std;


int main()
{
    Parser parser{};
    int c;
    do {
        c = getchar();
        parser.update(c);
    } while (c != EOF);
    cout <<  parser << endl;
    return 0;
}
