#include "Poll.h"
#include "Socket.h"
#include <unordered_map>
#include <list>

using namespace net;
using namespace std;

class Tcpecho : public Poll {
public:
    bool data(Socket *sock, Byte *buf, ssize_t size) override;
    bool halfclose(Socket *sock) override;
    bool connected(Socket *sock, const Address& addr, const ServerSocket *srv) override;
    bool drain(Socket *sock) override;
};

bool
Tcpecho::data(Socket *sock, Byte *buf, ssize_t size)
{
    sock->queue(buf, size);
    return true;
}

bool
Tcpecho::halfclose(Socket *sock)
{
    this->del(sock);
    delete sock;
    return true;
}

bool
Tcpecho::connected(Socket *sock, const Address& addr, const ServerSocket *srv)
{
    Socket *sockcopy = new Socket{*sock};
    add(sockcopy);
    return true;
}

bool
Tcpecho::drain(Socket *sock)
{
    sock->flush();
    return true;
}

int main(void)
{
    Tcpecho tcpecho{};
    ServerSocket srv{};
    srv.bind(Address{7779, "127.0.0.1"});
    tcpecho.add(&srv);
    srv.listen();
    for (;;){
        tcpecho.poll();
    }
    return 0;
}
