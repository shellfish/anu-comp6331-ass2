#include "Sunrpc.h"
#include <iostream>

using namespace std;
using namespace net;

int main(int argc, char *argv[])
{
    PortMapping mapping {
        100893, // nfs
        3,      // v3
        6,      // tcp
        19830
    };
    PortMapper pm{Address{111, argv[1]}};
    cout << pm.set(PortMapper::createMapping(100003, 3)) << endl;
    return 0;
}
