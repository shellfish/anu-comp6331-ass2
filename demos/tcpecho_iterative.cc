#include "Socket.h"
#include <iostream>

using namespace net;
using namespace std;

int main()
{
    ServerSocket srv{};
    Socket conn{-1};
    Address connAddr{};
    srv.bind(Address{7779, "127.0.0.1"});
    cout << "listening on 7779" << endl;
    srv.listen();
    Byte buf[1024];
    int len;
    while (true) {
        srv.acceptSync(conn, connAddr);
        while ((len = conn.readSync(buf, sizeof(buf))) > 0) {
            conn.writeSync(buf, len);
        }
        conn.close();
    }
    return 0;
}
