#include "Socket.h"
#include <iostream>
#include <assert.h>
using namespace net;
using namespace std;

int main()
{
    Address addr{7778, "127.0.0.1"};
    Address addr1{7778, "127.0.0.2"};
    Address addr2{7778, "127.0.0.3"};
    assert(addr1 < addr2);
    assert(addr2 > addr1);
    Socket sock{};
    cout << addr << endl;
    sock.connectSync(addr);
    Buffer buffer{};
    for (int i=0; i < 100; i++) {
        int b = sock.readSync(buffer);
        cout << b << endl;
    }
    cout << buffer << endl;
    return 0;
}
