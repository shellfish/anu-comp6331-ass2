include_directories(${CMAKE_SOURCE_DIR})

add_executable(simple_client simple_client.cc)
target_link_libraries(simple_client libnet)

add_executable(tcpecho_iterative tcpecho_iterative.cc)
target_link_libraries(tcpecho_iterative libnet)

add_executable(tcpecho_poll tcpecho_poll.cc)
target_link_libraries(tcpecho_poll libnet)

add_executable(inireader inireader.cc)
target_link_libraries(inireader libini)

add_executable(portmapper portmapper.cc)
target_link_libraries(portmapper libnet)
