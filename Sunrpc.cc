#include "Sunrpc.h"
#include <arpa/inet.h>
#include <cstdlib>
extern "C" {
#include <unistd.h>
#include <time.h>
}
using namespace std;
using namespace net;

static XdrUint initxid()
{
    struct timespec time;
    clock_gettime(CLOCK_REALTIME, &time);
    size_t diff_sub = sizeof(unsigned long) - 18;
    XdrUint xid = ((unsigned long)time.tv_sec << 18) |
                  (((unsigned long)time.tv_nsec >> diff_sub) << diff_sub);
    return xid;
}

SunRpc::SunRpc(const Address& target) : xid{initxid()}
{
    sock.connectSync(target);
}

SunRpc& SunRpc::operator<<(XdrUint uint)
{
    XdrUint nuint = htonl(uint);
    Byte* p = (Byte*)&nuint;
    for (auto i = 0; i < 4; i++) {
        *this << p[i];
    }
    return *this;
}

SunRpc& SunRpc::operator<<(Byte b)
{
    sendbuf.push_back(b);
    return *this;
}

void
SunRpc::call(const RpcMessage &msg)
{
    // send call request
    *this << msg;
    sock.writeSync(sendbuf.data(), sendbuf.size());
    sendbuf.clear();
    // receive reply message
    receiveReply();
}

void
SunRpc::receiveReply()
{
    XdrUint record_marker = popUint(); // record marker
    //cerr << "length " << (0x7fffffff & record_marker) << endl;
    popUint(); // xid
    if (popUint() != 1) { // msg_type : REPLY == 1
        throw RpcError{"not a reply"};
    }
    if (popUint() != 0){  // reply_stat : MSG_ACCEPTED == 0
        throw RpcError{"reply not accepted"};
    }
    // skip auth verf
    popUint();
    popUint();

    if (popUint() != 0) {// accept_stat == SUCCESS = 0
        throw RpcError{"reply not success"};
    }
}

SunRpc&
SunRpc::operator<<(const RpcMessage& msg)
{
    *this << (XdrUint)(0x80000000 | (40+msg.size))
          << xid++
          << (XdrUint)0 // msg type: call
          << (XdrUint)2 // rpcvers
          << msg.program << msg.version << msg.proc
          << (XdrUint)0 << (XdrUint)0 // opaque_auth cred = AUTH_NONE
          << (XdrUint)0 << (XdrUint)0; // opaque_auth verf = AUTH_NONE
    for (auto i = 0; i < msg.size; i++) {
        *this << msg.body[i];
    }
    return *this;
}

Byte
SunRpc::popByte()
{
    if (recvbuf.empty()) {
        Byte buf[1024];
        ssize_t x = sock.readSync(buf, sizeof(buf));
        //cerr << "read " << x << " bytes" << endl;
        for (auto i=0; i < x; i++) {
            recvbuf.push_back(buf[i]);
        }
    }
    Byte top = recvbuf.front();
    recvbuf.pop_front();
    return top;
}

void
SunRpc::skipOpaque()
{
    XdrUint length = popUint();
    for (auto i=0; i < length; i++) {
        popByte();
    }
}

XdrUint
SunRpc::popUint()
{
    Buffer buf{};
    for (auto i=0; i < 4; i++) {
        buf.push_back(popByte());
    }
    return ntohl(*(XdrUint*)buf.data());
}

bool
PortMapper::set(const PortMapping& mapping)
{
    call(makeInArgs(mapping, 1));
    return popBool();
}

bool
PortMapper::unset(const PortMapping& mapping)
{
    call(makeInArgs(mapping, 2));
    return popBool();
}

Port
PortMapper::getport(const PortMapping& mapping)
{
    call(makeInArgs(mapping, 3));
    return (Port)popUint();
}

RpcMessage
PortMapper::makeInArgs(const PortMapping& mapping, XdrUint proc)
{
    RpcMessage outmsg {};
    outmsg.program = 100000;
    outmsg.version = 2;
    outmsg.proc = proc;
    outmsg.body = (Byte*)&mapping;
    outmsg.size = 16;
    return outmsg;
}


PortMapping
PortMapper::createMapping(XdrUint program, XdrUint vers,
                          XdrUint port, XdrUint prot)
{
    PortMapping mp{};
    mp.prot = ntohl(prot);
    mp.port = ntohl(port);
    mp.prog = ntohl(program);
    mp.vers = ntohl(vers);
    return mp;
}
