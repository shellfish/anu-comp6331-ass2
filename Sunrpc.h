#ifndef SUNRPC_H
#define SUNRPC_H

#include "Socket.h"
#include <functional>
#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <stack>
#include <deque>

namespace net {

using XdrUint = std::uint32_t;

struct RpcMessage {
    XdrUint program;
    XdrUint version;
    XdrUint proc;
    Byte *body;
    std::size_t size; // body size
};

class SunRpc {
public:
    class RpcError : public std::runtime_error {using std::runtime_error::runtime_error; };
    SunRpc(const Address& target);
    // block until callback has been called
    void call(const RpcMessage &msg);
    // events:
protected:
    XdrUint popUint();
    bool popBool() { return popUint(); }
    Byte popByte();
private:
    XdrUint xid;
    Socket sock{};
    SunRpc& operator<<(XdrUint uint);
    SunRpc& operator<<(const RpcMessage& msg);
    SunRpc& operator<<(Byte b);
    void receiveReply();
    void skipOpaque();
    std::deque<Byte> recvbuf{};
    Buffer sendbuf{};
};

struct PortMapping {
    XdrUint prog;
    XdrUint vers;
    XdrUint prot;
    XdrUint port;
};

class PortMapper : public SunRpc {
public:
    PortMapper(const Address& target) : SunRpc{target} { }
    bool set(const PortMapping& mapping);
    bool unset(const PortMapping& mapping);
    Port getport(const PortMapping& mapping);
    static PortMapping createMapping(XdrUint program, XdrUint vers,
                              XdrUint port = 0, XdrUint prot = 6);
private:
    RpcMessage makeInArgs(const PortMapping& mapping, XdrUint proc);
};

}

#endif
