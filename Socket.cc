#include "Socket.h"
extern "C" {
#include <signal.h>
#include <errno.h>
#include <netdb.h>
}
#include <iostream>
#include <deque>
#include <cstring>
#include <algorithm>

using namespace std;

namespace net {

const char *errormsg()
{
    return sys_errlist[errno];
}

Address::Address(Port port, in_addr_t in_addr)
{
    if (in_addr == INADDR_NONE) {
        throw InvalidAddress{errormsg()};
    }
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = in_addr;
    addr.sin_port = htons(port);
}

bool
Address::operator<(const Address &other) const
{
    return ntohl(addr.sin_addr.s_addr) < ntohl(other.addr.sin_addr.s_addr);
}

bool
Address::operator>(const Address &other) const
{
    return ntohl(addr.sin_addr.s_addr) > ntohl(other.addr.sin_addr.s_addr);
}

bool
Address::operator==(const Address &other) const
{
    return ntohl(addr.sin_addr.s_addr) == ntohl(other.addr.sin_addr.s_addr);
}

static in_addr_t decimal_addr(const string& str)
{
    struct hostent * h = gethostbyname(str.c_str());
    if (h == nullptr || h->h_addrtype != AF_INET) {
        throw Address::InvalidAddress{"gethostbyname"};
    } else {
        for (char **p = h->h_addr_list; p != nullptr; p++) {
            return (*(in_addr_t*)(*p));
        }
    }
    throw Address::InvalidAddress{"gethostbyname"};
}

Address::Address(Port port, const string& str)
    : Address{port, decimal_addr(str)}
{

}

string
Address::toString() const
{
    char buf[16];
    inet_ntop(AF_INET, &addr.sin_addr, buf, sizeof(buf));
    return string{buf} + ":" + to_string(ntohs(addr.sin_port));
}

int
FileDescriptor::getStatusFlag()
{
    int val = fcntl(fd, F_GETFL, 0);
    if (val == -1)
        throw FcntlError{errormsg()};
    else
        return val;
}

int
FileDescriptor::dup() const
{
    int n = ::dup(fd);
    if (n >= 0) {
        return n;
    } else {
        throw DupError{errormsg()};
    }
}

FileDescriptor::FileDescriptor(FileDescriptor&& o) : FileDescriptor{o.fd}
{
    o.fd = -1;
}

FileDescriptor&
FileDescriptor::operator=(const FileDescriptor& o)
{
    close();
    fd = o.dup();
    return *this;
}

FileDescriptor&
FileDescriptor::operator=(FileDescriptor&& o)
{
    close();
    fd = o.fd;
    o.fd = -1;
    return *this;
}

void
FileDescriptor::setStatusFlag(int val)
{
    if (-1 == fcntl(fd, F_SETFL, val))
        throw FcntlError{errormsg()};
}

void
FileDescriptor::addStatusFlag(int val)
{
    setStatusFlag(getStatusFlag() | val);
}

void FileDescriptor::delStatusFlag(int val)
{
    setStatusFlag(getStatusFlag() & (val ^ 0));
}

Socket::Socket() :FileDescriptor{socket(AF_INET, SOCK_STREAM | SOCK_CLOEXEC, IPPROTO_TCP)}
{
    if (fd == -1) {
        throw InitFailure{errormsg()};
    }
}

class BlockingStateKeeper{
public:
    BlockingStateKeeper(Socket& sock, bool state)
        : flag{sock.getStatusFlag()}, sockref{&sock}
    {
        sock.setBlocking(state);
    }
    ~BlockingStateKeeper() { sockref->setStatusFlag(flag); }
private:
    int flag;
    Socket* sockref;
};


ssize_t
Socket::read(Byte *buffer, size_t count)
{
    BlockingStateKeeper bsk{*this, false};
    ssize_t t = ::read(fd, buffer, count);
    if (t > 0) {
        return t;
    } else if (t == 0) {
        return -1;
    } else if (errno == EWOULDBLOCK || errno == EAGAIN) {
        return 0;
    } else {
        throw ConnFailure{errormsg()};
    }
}

ssize_t
Socket::read(Buffer& buffer, const size_t count)
{
    Byte buf[count];
    ssize_t res = read(buf, count);
    if (res > 0) {
        buffer.insert(buffer.end(), buf, buf + res);
    }
    return res;
}

ssize_t
Socket::readSync(Byte *buffer, size_t count)
{
    BlockingStateKeeper bsk{*this, true};
    for (;;) {
        ssize_t t = ::read(fd, buffer, count);
        if (t > 0) {
            return t;
        } else if (t == 0) {
            return -1;
        } else if (errno == EINTR) {
            continue;
        } else {
            throw ConnFailure{errormsg()};
        }
    }
}


ssize_t
Socket::readSync(Buffer& buffer, const size_t count)
{
    Byte buf[count];
    ssize_t res = readSync(buf, count);
    if (res > 0) {
        buffer.insert(buffer.end(), buf, buf + res);
    }
    return res;
}


ssize_t
Socket::write(const Byte *buf, size_t count)
{
    BlockingStateKeeper bsk{*this, false};
    ssize_t t = ::write(fd, buf, count);
    if (t == -1) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
            return 0; // blocking write when set as nonblocking
        } else {
            throw ConnFailure{errormsg()};
        }
    } else {
        return t;
    }
}

void
Socket::writeSync(const Byte *buf, size_t count)
{
    if (count <= 0) return;
    BlockingStateKeeper bsk{*this, true};
    size_t hasWriten = 0;
    while (hasWriten != count) {
        ssize_t t = ::write(fd, buf + hasWriten, count - hasWriten);
        if (t >= 0) {
            hasWriten += t;
        } else if (errno == EINTR) {
            continue;
        } else {
            throw ConnFailure{errormsg()};
        }
    }
}

struct Block {
    Byte *buf;
    size_t size;
};

class WriteCache {
private:
    std::deque<Block> blocks{};
    int index {0};
public:
    ~WriteCache();
    Byte *block() const;
    size_t length() const;
    void append(const Byte *newblock, const size_t size);
    void flush(size_t hasWriten);
    bool empty() const { return blocks.empty(); }
};


WriteCache::~WriteCache()
{
    for (auto &i : blocks) {
        delete[] i.buf;
    }
}

Byte*
WriteCache::block() const
{
    return blocks.at(0).buf + index;
}

size_t
WriteCache::length() const
{
    return blocks.at(0).size - index;
}

void
WriteCache::append(const Byte *buf, const size_t size)
{
    Byte *newbuf = new Byte[size];
    copy(buf, buf+size, newbuf);
    blocks.push_back(Block{newbuf, size});
}

void
WriteCache::flush(size_t hasWriten)
{
    if (blocks.empty()) return;
    index += hasWriten;
    if (index == blocks.at(0).size) {
        blocks.pop_front();
        index = 0;
    }
}

Socket::~Socket() { if (cache!=nullptr) delete cache;}

bool
Socket::nonEmptyCache()
{
    if (cache == nullptr) {
        return false;
    } else {
        return !cache->empty();
    }
}

void Socket::queue(const Byte *buf, size_t count)
{
    ssize_t r = 0;
    if (flush()) {
        if (buf) {
            r = write(buf, count);
            if (r == count) {
                return;
            }
        } else {
            // close local writing end if buf is nullptr
            ::shutdown(fd, SHUT_WR);
        }
    }
    if (cache == nullptr) {
        cache = new WriteCache{};
    }
    if (buf) {
        cache->append(buf + r, count - r);
    } else {
        cache->append(nullptr, 0);
    }
}

bool Socket::flush()
{
    if (cache == nullptr || cache->empty()) {
        return true;
    }
    if (hasConnected) {
        while (!cache->empty()) {
            if (cache->block()) {
                ssize_t r = write(cache->block(), cache->length());
                if (r > 0) {
                    cache->flush(r);
                } else {
                    return false;
                }
            } else {
                ::shutdown(fd, SHUT_WR);
                delete cache;
                cache = nullptr;
                return true;
            }
        }
        return true;
    } else {
        return false;
    }
}

void
Socket::connectSync(const Address& addr)
{
    BlockingStateKeeper bsk{*this, true};
    if (::connect(fd,
                  reinterpret_cast<const struct sockaddr*>(&addr.addr),
                  sizeof(sockaddr_in)) == -1) {
        throw ConnFailure{errormsg()};
    } else {
        setConnected();
    }
}

bool
Socket::connect(const Address& addr)
{
    BlockingStateKeeper bsk{*this, false};
    int result = ::connect(fd,
            reinterpret_cast<const struct sockaddr*>(&addr.addr),
            sizeof(sockaddr_in));
    if (result == 0) {
        setConnected();
        return true;
    } else if (errno != EINPROGRESS) {
        throw ConnFailure{errormsg()};
    } else {
        return false;
    }
}

void Socket::bind(const Address &addr)
{
    if (::bind(fd,
           reinterpret_cast<const struct sockaddr*>(&addr.addr),
           sizeof(addr.addr)) == -1)
        throw BindFailure{errormsg()};
}

//man exports(5)
    //This option requires that requests originate on an Internet port
    //less  than IPPORT_RESERVED (1024). This option is on by default.
    //To turn it off, specify insecure.
void Socket::bindReserved()
{
    // try from 1023 down to 1
    Port port = 1023;
    do {
        try {
            bind(Address{port});
            break;
        } catch (BindFailure) {
            port--;
        }
    } while (port != 0);
    if (port == 0) {
        throw BindFailure{"cannot bind to any reserved ports"};
    }
}

ostream& operator<<(ostream& stream, const Address& a) {
    return stream << a.toString();
}

ostream& operator<<(ostream& stream, const Buffer& a) {
    for (auto c : a)
        stream << c;
    return stream;
}


void
ServerSocket::listen(int backlog)
{
    if (::listen(fd, backlog) == -1) {
        throw ListenFailure{errormsg()};
    }
}


Socket&
Socket::operator=(const Socket& other)
{
    FileDescriptor::operator=((const FileDescriptor&)other);
    hasConnected = other.hasConnected;
    return *this;
}

Socket&
Socket::operator=(Socket&& other)
{
    FileDescriptor::operator=((FileDescriptor&&)other);
    other.fd = -1;
    hasConnected = other.hasConnected;
    other.hasConnected = false;
    return *this;
}


bool
ServerSocket::accept(Socket& insock, Address& inaddr)
{
    BlockingStateKeeper bsk{*this, false};
    socklen_t len = sizeof(inaddr.addr);
    int r = ::accept(fd, (sockaddr*)&inaddr.addr, &len);
    if (r >= 0) {
        insock = Socket{r};
        insock.setConnected();
        return true;
    } else if (errno == EAGAIN || errno == EWOULDBLOCK) {
        return false;
    } else {
        throw AcceptFailure{errormsg()};
    }
}


void
ServerSocket::acceptSync(Socket& insock, Address& inaddr)
{
    BlockingStateKeeper bsk{*this, true};
    socklen_t len = sizeof(inaddr.addr);
    for (;;) {
        int r = ::accept(fd, (sockaddr*)&inaddr.addr, &len);
        if (r >= 0) {
            insock = std::move(Socket{r});
            insock.setConnected();
            break;
        } else if (errno != EINTR) {
            throw AcceptFailure{errormsg()};
        }
    }
}

void
Socket::reset()
{
    struct linger l;
    l.l_onoff = 1;
    l.l_linger = 0;
    int r = setsockopt(fd, SOL_SOCKET, SO_LINGER, &l, sizeof(l));
    if (r == -1) {
        throw ConnFailure{errormsg()};
    } else {
        close();
    }
}

} // end namespace net
