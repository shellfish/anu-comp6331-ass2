#include "NFSState.h"
#include <assert.h>
#include <arpa/inet.h>
#include <iostream>
using namespace net;
using namespace std;


void
StatData::increase(NFSState::OPCODE opcode)
{
    try {
        map.at(opcode)++;
    } catch (out_of_range) {
        map.emplace(opcode, 1);
    }
}

void
StatGroup::increase (NFSState::OPCODE opcode, int uid)
{
    try {
        map.at(uid).increase(opcode);
    } catch (out_of_range) {
        map.emplace(uid, StatData{});
        map.at(uid).increase(opcode);
    }
}


void NFSState::update(Byte byte)
{
    if (index == frag_size + 4 && is_lastfrag) {
        stage = Stage::HEADER;
        index = 0;
    }

    //cout << "index at " << index << endl;
    if (stage == Stage::HEADER) {
        switch (index) {
        // unisgned int, record marker
        case 0: case 1: case 2:
            buffer.push_back(byte);
            break;
        case 3:
            buffer.push_back(byte);
            is_lastfrag = !!(buffer[0] == 0x80);
            buffer[0] &= 0x7f;
            frag_size = popUInt32();
            break;
        // unsinged int, xid, 4,5,6,7
        // enum msg_type
        case 8: case 9: case 10:
            buffer.push_back(byte);
            break;
        case 11:
            buffer.push_back(byte);
            if (popUInt32() != 0) { abandonRecord(); }
            break;
        // rpcvers;       /* must be equal to two (2) */
        case 12: case 13: case 14:
            buffer.push_back(byte);
            break;
        case 15:
            buffer.push_back(byte);
            if (popUInt32() != 2) { abandonRecord(); }
            break;
        // unsigned int, prog
        case 16: case 17: case 18:
            buffer.push_back(byte);
            break;
        case 19:
            buffer.push_back(byte);
            if (popUInt32() != 100003) { abandonRecord(); }
            break;
        // unsigned int, vers
        case 20: case 21: case 22:
            buffer.push_back(byte);
            break;
        case 23:
            buffer.push_back(byte);
            if (popUInt32() != 3) {  abandonRecord(); }
            break;
        // unsigned int, proc
        case 24: case 25: case 26:
            buffer.push_back(byte);
            break;
        case 27:
            buffer.push_back(byte);
            proc = popUInt32();
            break;
        // opaque_auth cred
        // cred::flavor : auth_flavor
        case 28: case 29: case 30:
            buffer.push_back(byte);
            break;
        case 31:
            buffer.push_back(byte);
            if (popUInt32() == 1) { // AUTH_SYS
                stage = Stage::AUTH;
                authoffset = 0;
            } else {
                stat->call(proc, -1);
                abandonRecord();
            }
            break;
        default:
            break;
        }
    } else if (stage == Stage::AUTH) {
        switch (authoffset) {
            // opaque length
            case 0: case 1: case 2: case 3: break;
            // case stamp
            case 4: case 5: case 6: case 7: break;
            case 8: case 9: case 10: case 11:
                buffer.push_back(byte);
                if (authoffset == 11) {
                    machinename_length = popUInt32();
                }
                break;
            default:
                if (authoffset >= 12 +  machinename_length
                    && authoffset < 15 + machinename_length) {
                    buffer.push_back(byte);
                } else if (authoffset == 15 + machinename_length) {
                    buffer.push_back(byte);
                    UINT32 uid = popUInt32();
                    stat->call(proc, uid);
                    abandonRecord();
                }
                break;
        }
        authoffset += 1;
    }

    index += 1;
}

NFSState::UINT32
NFSState::popUInt32()
{
    assert(buffer.size() == 4);
    Byte *d = buffer.data();
    UINT32 i = ntohl(*(UINT32*)d);
    buffer.clear();
    return i;
}

void
NFSStatistics::call(NFSState::OPCODE opcode, int uid)
{
    // cout << "call " << optab(opcode) << " by uid(" << uid << ")" << endl;
    data.increase(opcode, uid);
}

const char *
NFSStatistics::optab(NFSState::OPCODE opcode)
{
    switch (opcode) {
        case 0:
            return "NULL";
        case 1:
            return "GETATTR";
        case 2:
            return  "SETATTR";
        case 3:
            return  "LOOKUP";
        case 4:
            return  "ACCESS";
        case 5:
            return "READLINK";
        case 6:
            return  "READ";
        case 7:
            return  "WRITE";
        case 8:
            return  "CREATE";
        case 9:
            return "MKDIR";
        case 10:
            return  "SYMLINK";
        case 11:
            return  "MKNOD";
        case 12:
            return "REMOVE";
        case 13:
            return "RMDIR";
        case 14:
            return  "RENAME";
        case 15:
            return  "LINK";
        case 16:
            return  "READDIR";
        case 17:
            return "READDIRPLUS";
        case 18:
            return  "FSSTAT";
        case 19:
            return  "FSINFO";
        case 20:
            return  "PATHCONF";
        case 21:
            return "COMMIT";
        default:
            return "UNKNOWN";
    }
}
void
NFSState::abandonRecord()
{
    stage = Stage::REST;
}

string
NFSStatistics::report()
{
    string r = data.report();
    data.clear();
    return r;
}

string
StatData::report()
{
    string result {};
    for (auto &i : map) {
        result += "  ";
        result += string{""} + NFSStatistics::optab(i.first) + " : ";
        result += to_string(i.second) + "\n";
    }
    return result;
}

string
StatGroup::report()
{
    string result {};
    for (auto &i :map) {
        if (i.first == -1) {
            result += string{"anonymous"} + "\n";
        } else {
            result += string{"uid "} + to_string(i.first) + "\n";
        }
        result += i.second.report();
    }
    return result;
}
