#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include <string>
#include <stdexcept>
#include <ostream>
#include <vector>
#include <cstdint>
#include <climits>
extern "C" {
#include <sys/socket.h> // sockaddr
#include <netinet/in.h> // sockaddr_in etc.
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
}
#include <iostream>

namespace net {


using Port = std::uint16_t;
using Byte = unsigned char;
using Buffer = std::vector<Byte>;

class Socket;
class ServerSocket;
class Poll;
class Address {
    friend class Socket;
    friend class ServerSocket;
public:
    class InvalidAddress : public std::runtime_error {
        using std::runtime_error::runtime_error;};
    Address(Port port, const std::string& addrstr);
    Address(Port port = 0, in_addr_t in_addr = INADDR_ANY);
    bool operator<(const Address &other) const;
    bool operator>(const Address &other) const;
    bool operator==(const Address &other) const;
    bool operator<=(const Address &other) const {
        return *this < other || *this == other; };
    bool operator>=(const Address &other) const {
        return *this > other || *this == other ;};
    std::string toString() const;
private:
    struct sockaddr_in addr;
};

class FileDescriptor {
friend class Poll;
public:
    class FcntlError : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };
    class DupError: public std::runtime_error {
        using std::runtime_error::runtime_error;
    };
    FileDescriptor(int newfd) : fd{newfd} {}
    ~FileDescriptor() {close(); }
    FileDescriptor(const FileDescriptor& o) : FileDescriptor{o.dup()} {}
    FileDescriptor(FileDescriptor&& o);
    FileDescriptor& operator=(const FileDescriptor& o);
    FileDescriptor& operator=(FileDescriptor&& o);
    void close() { if (fd >= 0) { ::close(fd); fd = -1; }}
    int getStatusFlag();
    void setStatusFlag(int val);
    void addStatusFlag(int val);
    void delStatusFlag(int val);
    int val() const { return fd; }
    int dup() const;
protected:
    int fd {-1};
};


class WriteCache;
class ServerSocket;
class Socket : public FileDescriptor {
public:
    class InitFailure: public std::runtime_error {
        using std::runtime_error::runtime_error;};
    class ConnFailure: public std::runtime_error {
        using std::runtime_error::runtime_error;};
    class BindFailure: public std::logic_error {
        using std::logic_error::logic_error;};

    Socket();
    // when fd >= 0, create new socket for fd
    // when fd < 0, create a placeholder
    Socket(int fd) : FileDescriptor{fd} {};
    Socket(const Socket& sock)
        : FileDescriptor{sock.dup()}, hasConnected{sock.hasConnected} {}
    Socket(Socket&& sock)
        : FileDescriptor{sock.fd}, hasConnected{sock.hasConnected} {
            sock.fd=-1;
            sock.hasConnected = false;
        }
    Socket& operator=(const Socket& other);
    Socket& operator=(Socket&& other);
    ~Socket();

    // return false: in progress
    // return true: connected
    bool connect(const Address& addr);
    void connectSync(const Address& addr);

    // return -1: end of file;
    // return 0: would block;
    // return n>0: number of bytes been read
    static const int DEFAULT_INBUF_SIZE = 2048;
    ssize_t read(Byte *buf, size_t count);
    ssize_t readSync(Byte *buf, size_t count);
    ssize_t read(Buffer& buf, const size_t count = DEFAULT_INBUF_SIZE);
    ssize_t readSync(Buffer& buf, const size_t count = DEFAULT_INBUF_SIZE);
    void bind(const Address& to);
    void bindReserved();

    // return 0: would block
    // return n>0 (may less than count): bytes been writen
    ssize_t write(const Byte *buf, size_t count);
    // promise to write the whole buffer
    void writeSync(const Byte *buf, size_t count);

    // send TCP rst, will call close() internally
    void reset();

    // try writing when os buffer is available
    // otherwise push back into internal queue
    void queue(const Byte *buf, size_t count);
    // return true: all cache flushed or empty queue
    // return false: block before queue becomes empty
    bool flush();
    bool nonEmptyCache();

    void setBlocking(bool b) {
        if (b) delStatusFlag(O_NONBLOCK); else addStatusFlag(O_NONBLOCK);}

    void setConnected() { if (!hasConnected) hasConnected = true; }
private:
    WriteCache *cache {nullptr};
    bool hasConnected {false};
};


class ServerSocket : public Socket {
public:
    class ListenFailure : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };
    class AcceptFailure : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };
    void listen(int backlog = 20);
    bool accept(Socket& insock, Address& inaddr);
    void acceptSync(Socket& insock, Address& inaddr);
};

std::ostream& operator<<(std::ostream& stream, const Address& a);
std::ostream& operator<<(std::ostream& stream, const Buffer& a);

} // end namespace net

#endif
