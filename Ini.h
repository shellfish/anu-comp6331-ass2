#ifndef INI_H
#define INI_H

#include <string>
#include <unordered_map>
#include <stack>
#include <stdexcept>
#include <ostream>

namespace ini {

enum class Term {
    // Nonterminals:
    SYMBOL,  NEWLINE,  ASSIGN, LBRACE, RBRACE, END,
    // Terminals:
    INI, EXPRLIST, GROUPLIST, GROUP, SECTION, EXPR
};

class Lexer {
public:
    enum class State {
        NORMAL, COMMENT, QUOTE
    };

    void update(int c);
    void update(const char *str);
    void update(const std::string& str);
    void end(); // update using EOF
    virtual void symbol(Term lexicon, const std::string& value) = 0;
protected:
    void reset();
private:
    std::string buffer{};
    State state{State::NORMAL};
    void complete_symbol();
    Term lastlex{Term::NEWLINE};
    void _symbol(Term lexicon, const std::string& value);
};

using Ini = std::unordered_map<std::string, std::string>;

class Parser : public Lexer {
    friend std::ostream& operator<<(std::ostream& stream, const Parser& parser);
public:
    class SyntaxError : public std::exception {};
    Parser() { tstack.push(Term::INI); }
    void symbol(Term lexicon, const std::string& value) override;
    const Ini& ini() { return hash; }
    const char *get(const std::string& key) const;
    void reset(); // reset parser state, waiting for a new ini
private:
    Ini hash{};
    void match(Term lexicon, const std::string &value);
    std::stack<Term> tstack{};
    bool inside_braces{false};
    bool atleftside{true}; // left of =
    std::string leftval{};
    std::string rightval{};
    std::string secval{};
};

std::ostream& operator<<(std::ostream& stream, Term term);

}

#endif
