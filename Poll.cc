#include "Poll.h"
#include <iostream>
using namespace std;

namespace net {

void Poll::add(Socket *sock)
{
    sockmap.insert(make_pair<int, Socket*>(sock->val(), move(sock)));
}

void Poll::add(ServerSocket *srv)
{
    servermap.insert(make_pair<int, ServerSocket*>(srv->val(), move(srv)));
}


void Poll::del(Socket* sock)
{
    sockmap.erase(sock->val());
}

void Poll::del(ServerSocket *srv)
{
    servermap.erase(srv->val());
}

void Poll::rebuild_pollfds()
{
    pollfds.clear();
    for (auto &s: sockmap) {
        pollfd pfd{};
        pfd.fd = s.first;
        pfd.events = POLLIN;
        if (s.second->nonEmptyCache()) {
            pfd.events |= POLLOUT;
        }
        pollfds.push_back(pfd);
    }

    for (auto &s: servermap) {
        pollfd pfd{};
        pfd.fd = s.first;
        pfd.events = POLLIN;
        pollfds.push_back(pfd);
    }
}


void Poll::poll(int timeout)
{
    rebuild_pollfds();
    int count = ::poll(pollfds.data(), pollfds.size(), timeout);
    if (count >  0) {
        for (auto &pfd : pollfds) {
            if ((pfd.revents & POLLOUT) || (pfd.revents & POLLIN)) {
                decltype(sockmap)::iterator i = sockmap.find(pfd.fd);
                if (i != sockmap.end()) {
                    // is normal socket
                    Socket* sock = i->second;
                    Byte buf[Socket::DEFAULT_INBUF_SIZE];
                    if (pfd.revents & POLLIN) {
                        while (true) {
                            ssize_t t = sock->read(buf, sizeof(buf));
                            if (t > 0) {
                                // cout << "data" << endl;
                                data(sock, buf, t);
                            } else if (t == -1) { // EOF
                                // cout << "halfclose" << endl;
                                halfclose(sock);
                                break;
                            } else { // is blocking
                                break;
                            }
                        }
                    }
                    if (pfd.revents & POLLOUT) {
                        sock->setConnected();
                        // cout << "drain" << endl;
                        drain(sock);
                    }
                } else if ((pfd.revents & POLLIN)) {
                    decltype(servermap)::iterator j = servermap.find(pfd.fd);
                    if (j != servermap.end()) {
                        ServerSocket* srv = j->second;
                        Socket sock;
                        Address addr;
                        bool r = srv->accept(sock, addr);
                        if (!r) {
                            throw ServerSocket::AcceptFailure{
                                "cannot accept inside poll"};
                        }
                        // cout << "connected" << endl;
                        connected(&sock, addr, srv);
                    }
                }

                if (pfd.revents) {
                    count -= 1;
                    if (count == 0) break;
                }
            }
        }
    } else if (count == -1) {
        if (errno == EINTR) {
            cout << "SINGAL" << endl;
        }
    } else {
    
    }
}




} // end namespace net
