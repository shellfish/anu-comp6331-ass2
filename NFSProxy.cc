#include "NFSProxy.h"
#include <unordered_set>
#include <string>

using namespace std;

namespace net {

NFSProxy::NFSProxy(const Address &local, const Address& remote)
    : remoteAddr{remote}
{
    proxy_srv.bind(local);
    proxy_srv.listen();
    add(&proxy_srv);
}

bool
NFSProxy::data(Socket *sock, Byte *buf, ssize_t size)
{
    try {
        Socket * bsock = fbmap.at(sock);
        NFSState &state = states.at(sock);
        for (auto i = 0; i < size; i++) {
            state.update(buf[i]);
        }
        bsock->queue(buf, size);
        return true;
    } catch (out_of_range) {
        try {
            Socket * fsock = bfmap.at(sock);
            fsock->queue(buf, size);
            return true;
        } catch (out_of_range) {
            return false;
        }
    }
}

bool
NFSProxy::connected(Socket *sock, const Address& addr, const ServerSocket *srv)
{
    if (srv == &proxy_srv) {
        if (addressValidator(addr)) {
            Socket *fsock = new Socket{move(*sock)};
            Socket *bsock = new Socket{};
            if (useReserved) {
                bsock->bindReserved();
            }
            bsock->connect(remoteAddr);
            add(bsock);
            add(fsock);
            bfmap.emplace(bsock, fsock);
            fbmap.emplace(fsock, bsock);

            states.emplace(fsock, NFSState{&statistics});
        } else {
            cerr << "reject nfs request from " << addr << endl;
            sock->reset();
        }
        return true;
    } else {
        return false;
    }
}

bool
NFSProxy::halfclose(Socket *sock)
{
    try { // if is forward(active) connection
        Socket *bsock = fbmap.at(sock);
        this->del(sock);
        this->del(bsock);
        fbmap.erase(sock);
        bfmap.erase(bsock);
        delete sock;
        delete bsock;
        states.erase(sock);
        return true;
    } catch (out_of_range) { // if is backward(passive) connection
        try {
            Socket *fsock = bfmap.at(sock);
            this->del(sock);
            this->del(fsock);
            bfmap.erase(sock);
            fbmap.erase(fsock);
            delete sock;
            delete fsock;
            states.erase(fsock);
            return true;
        } catch (out_of_range) {
            return false;
        }
    }
}

bool
NFSProxy::drain(Socket *sock)
{
    sock->flush();
    return true;
}


NFSProxyStat::NFSProxyStat(const Address& local,
                           const Address& remote,
                           const Address& telnet)
    : NFSProxy{local, remote}
{
    telnet_srv.bind(telnet);
    telnet_srv.listen();
    add(&telnet_srv);
}

bool
NFSProxyStat::connected(Socket *sock, const Address& addr, const ServerSocket *srv)
{
    if (NFSProxy::connected(sock, addr, srv)) {
        return true;
    } else if (srv == &telnet_srv) {
        string report = statistics.report();
        sock->queue((Byte*)report.c_str(), report.size());
        sock->queue(nullptr, 0);
        set.emplace(sock);
        return true;
    } else {
        return false;
    }
}

bool
NFSProxyStat::halfclose(Socket *sock)
{
    if (NFSProxy::halfclose(sock)) {
        return true;
    } else if (set.find(sock) != set.end()) {
        delete sock;
        del(sock);
        return true;
    } else {
        return false;
    }
}


NFSProxyMount::NFSProxyMount(const Address &local, const Address& remote,
                             const Address &telnet,
                             const Address &mountlocal, const Address &mountremote)
    : NFSProxyStat{local, remote, telnet}, remoteAddr{mountremote}
{
    proxy_srv.bind(mountlocal);
    proxy_srv.listen();
    add(&proxy_srv);
}

bool
NFSProxyMount::data(Socket *sock, Byte *buf, ssize_t size)
{
    if (NFSProxyStat::data(sock, buf, size)) {
        return true;
    } else {
        try {
            Socket * bsock = fbmap.at(sock);
            bsock->queue(buf, size);
            return true;
        } catch (out_of_range) {
            try {
                Socket * fsock = bfmap.at(sock);
                fsock->queue(buf, size);
                return true;
            } catch (out_of_range) {
                return false;
            }
        }
    }
}

bool
NFSProxyMount::connected(Socket *sock, const Address& addr, const ServerSocket *srv)
{
    if (NFSProxyStat::connected(sock, addr, srv)) {
        return true;
    } else if (srv == &proxy_srv) {
        if (addressValidator(addr)) {
            Socket *fsock = new Socket{move(*sock)};
            Socket *bsock = new Socket{};
            if (useReserved) {
                bsock->bindReserved();
            }
            bsock->connect(remoteAddr);
            add(bsock);
            add(fsock);
            bfmap.emplace(bsock, fsock);
            fbmap.emplace(fsock, bsock);
        } else {
            cerr << "reject mount request from " << addr << endl;
            sock->reset();
        }
        return true;
    } else {
        return false;
    }
}

bool
NFSProxyMount::halfclose(Socket *sock)
{
    if (NFSProxyStat::halfclose(sock)) {
        return true;
    } else {
        try { // if is forward(active) connection
            Socket *bsock = fbmap.at(sock);
            this->del(sock);
            this->del(bsock);
            fbmap.erase(sock);
            bfmap.erase(bsock);
            delete sock;
            delete bsock;
            return true;
        } catch (out_of_range) { // if is backward(passive) connection
            try {
                Socket *fsock = bfmap.at(sock);
                this->del(sock);
                this->del(fsock);
                bfmap.erase(sock);
                fbmap.erase(fsock);
                delete sock;
                delete fsock;
                return true;
            } catch (out_of_range) {
                // default action, may be better to crash?
                delete sock;
                del(sock);
                return true;
            }
        }
    }
}

} // end namespace
