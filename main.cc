#include "NFSProxy.h"
#include "Ini.h"
#include "Sunrpc.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <functional>
#include <csignal>
extern "C" {
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
}
using namespace net;
using namespace ini;
using namespace std;

const static char *default_ini = R"==(
[net]

# because mount address and port are no fixed,
# so they must be specified in your own ini file
local_addr = 127.0.0.1:6330
telnet_addr = 127.0.0.1:6331
remote_addr = 127.0.0.1:2049
)==";

static void splitAddress(const string& instr, string* outaddr, Port* outport) {
    size_t pos = instr.find_first_of(":");
    if (pos == string::npos) {
        cerr << instr << endl;
        throw runtime_error{"invalid address format"};
    } else {
        if (outaddr) {
            *outaddr = instr.substr(0, pos);
        }
        if (outport) {
            *outport = stoi(instr.substr(pos+1, instr.size()));
        }
    }
}

static Address
parseAddress(const string& instr) {
    string addr{};
    Port port;
    splitAddress(instr, &addr, &port);
    return Address{port, addr};
}

static bool isExiting = false;

void sigintHandler(int sig_num)
{
    signal(SIGINT, sigintHandler);
    isExiting = true;
}


int main(int argc, char *argv[])
{
    Parser conf{};
    conf.update(default_ini);
    conf.end();
    conf.reset();

    if (argc == 2) {
        ifstream ifs;
        ifs.open(argv[1], ifstream::in);
        if (!ifs.is_open()) {
            cerr << "ignore unreadable configuration file: " << argv[1] << endl;
        } else {
            char c = ifs.get();
            while (ifs.good()) {
                conf.update(c);
                c = ifs.get();
            }
            conf.end();
            ifs.close();
            cout << "using configuration: " << argv[1] << endl;
        }
    }

    Address remote = parseAddress(conf.get("net.remote_addr"));
    Address local = parseAddress(conf.get("net.local_addr"));
    Address telnet = parseAddress(conf.get("net.telnet_addr"));

    NFSProxy *pserver;
    if (conf.get("net.mount_remote_addr")
        && conf.get("net.mount_local_addr")) {
        pserver = new NFSProxyMount{local, remote, telnet,
            parseAddress(conf.get("net.mount_local_addr")),
            parseAddress(conf.get("net.mount_remote_addr"))};
    } else {
        pserver = new NFSProxyStat{local, remote, telnet};
    }

    if (conf.get("access.use_reserved_ports")) {
        if (string{conf.get("access.use_reserved_ports")} != string("no")) {
            pserver->setUseReserved();
        }
    }

    // do address filtering
    AddressValidator validator = DefaultValidator{};
    if (conf.get("access.lower_bound")) {
        Address lower_bound = Address{0, conf.get("access.lower_bound")};
        validator = [lower_bound](const Address &a) {
            return a >= lower_bound;
        };
    }
    if (conf.get("access.upper_bound")) {
        Address upper_bound = Address{0, conf.get("access.upper_bound")};
        AddressValidator origValidator = move(validator);
        validator = [upper_bound, origValidator](const Address &a) {
            return origValidator(a) && (a <= upper_bound);
        };
    }
    pserver->setNfsValidator(validator);
    {
        NFSProxyMount *pm = dynamic_cast<NFSProxyMount*>(pserver);
        if (pm != nullptr) {
            pm->setMountValidator(validator);
        }
    }

    pid_t pid = fork();
    if (pid == 0) {
        // try to register port numbers
        string nfs_addr;
        Port nfs_port;
        bool nfs_registed = false;

        string mount_addr;
        Port mount_port;
        bool mount_registed = false;
        PortMapping nfs_mapping {
            ntohl(100003),
            ntohl(3),
            ntohl(6),
            0
        };

        PortMapping mount_mapping {
            ntohl(100005),
            ntohl(3),
            ntohl(6),
            0
        };
        try {
            // register nfs
            splitAddress(conf.get("net.local_addr"), &nfs_addr, &nfs_port);
            nfs_mapping.port = ntohl(nfs_port);
            PortMapper pm{Address{111, nfs_addr}};
            pm.set(nfs_mapping);
            nfs_registed = true;

            if (conf.get("net.mount_local_addr")) {
                splitAddress(conf.get("net.mount_local_addr"), &mount_addr, &mount_port);
                mount_mapping.port = ntohl(mount_port);
                PortMapper pm{Address{111, mount_addr}};
                pm.set(mount_mapping);
                mount_registed = true;
            }

        } catch (SunRpc::RpcError) {
            cerr << "failed to register nfs proxy on portmapper" << endl;
        }

        signal(SIGINT, sigintHandler);
        while (true) {
            pause();
            if (isExiting) {
                break;
            }
        }
        if (nfs_registed) {
            PortMapper pm{Address{111, nfs_addr}};
            pm.unset(nfs_mapping);
            if (mount_registed) {
                PortMapper pm{Address{111, mount_addr}};
                pm.unset(mount_mapping);
            }
        }
        exit(0);

    } else {
        signal(SIGINT, sigintHandler);
        cerr << "press Ctrl+C to exit" << endl;
        for (;;) {
            pserver->poll();
            if (isExiting) {
                break;
            }
        }
    }

    delete pserver;
    wait(0);
    return 0;
}
