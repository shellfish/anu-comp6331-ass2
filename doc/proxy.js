var dest_port = process.argv[2] || 5001
var src_port = process.argv[3] || 5000
var net = require('net')
var fstream =
  require('fs').createWriteStream('log',
                                  {flags:'a', encoding:null, end: false})
var counter = 0

var srv = net.createServer(function(c) {
  var backcon = net.connect(src_port)
  counter += 1
  // seems like linux reuse sockets to connect to the same nfs server
  console.log('new incoming connection: concurrency: ', counter)
  backcon.pipe(c)
  c.pipe(backcon)
  c.pipe(fstream, {end: false})
  c.on('end', function() { counter -= 1})
})
srv.listen(dest_port)
