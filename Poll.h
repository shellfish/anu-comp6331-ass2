#ifndef POLL_H
#define POLL_H

#include "Socket.h"
#include <unordered_map>
#include <vector>
extern "C" {
#include <poll.h>
}
#include <iostream>

namespace net {

class Poll {
    public:
        Poll() {};
        virtual ~Poll() {};
        void add(Socket* sock);
        void add(ServerSocket *srv);
        void del(Socket* sock);
        void del(ServerSocket *srv);

        // event functions:
        // return true if events is dispatched
        // otherwise return false to indicate I'm not able to handle it
        // by myself and expect (ie. a subclass) to do further process
        virtual bool data(Socket *sock, Byte *buf, ssize_t size) = 0;
        virtual bool halfclose(Socket *sock) = 0;
        virtual bool connected(Socket *sock, const Address& addr, const ServerSocket* srv) = 0;
        virtual bool drain(Socket *sock) = 0;

        // will block defaultly
        void poll(int timeout = -1);
    protected:
        std::unordered_map<int, Socket*> sockmap{};
        std::unordered_map<int, ServerSocket*> servermap{};
        std::vector<pollfd> pollfds{};
        void rebuild_pollfds();
};

} // end namespace net

#endif
