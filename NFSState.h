// one state per incoming connection
// will be fed byte stream, and maintain the statistics about
//   this connection

#ifndef NFS_STATE_H
#define NFS_STATE_H

#include "Socket.h"
#include <string>
#include <iostream>
#include <cstdint>
#include <unordered_map>

class NFSStatistics;

class NFSState {
    enum class Stage {HEADER, AUTH, REST};
public:
    using UINT32 = std::uint32_t;
    using OPCODE = UINT32;
    NFSState(NFSStatistics *s) : stat{s} {};
    void update(net::Byte byte);
    UINT32 popUInt32();
private:
    int index{0}; // offset inside record
    int authoffset{};
    bool is_lastfrag{};
    UINT32 frag_size{0};
    UINT32 proc{};
    UINT32 machinename_length{};
    net::Buffer buffer{};
    NFSStatistics* stat;
    Stage stage{Stage::HEADER};
    void abandonRecord();
};

class StatData {
private:
    std::unordered_map<NFSState::OPCODE, int> map{};
public:
    void increase (NFSState::OPCODE opcode);
    std::string report();
};

class StatGroup {
private:
    std::unordered_map<int, StatData> map{};
public:
    void increase (NFSState::OPCODE, int uid);
    std::string report();
    void clear() { map.clear(); }
};

class NFSStatistics {
public:
    static const char *optab(NFSState::OPCODE opcode);
    // export statistics and clear current data
    std::string report();
    void call(NFSState::OPCODE opcode, int uid);
private:
    StatGroup data{};
};


#endif
