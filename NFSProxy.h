#ifndef NFSPROXY_H
#define NFSPROXY_H

#include "NFSState.h"
#include "Poll.h"
#include "Socket.h"
#include <unordered_map>
#include <unordered_set>
#include <functional>

namespace net {

using AddressValidator = std::function<bool(const Address&)>;

class DefaultValidator {
public:
    bool operator()(const Address& a) { return true;}
};

// proxy nfs requests and collects statistics per se
class NFSProxy : public Poll {
protected:
    NFSStatistics statistics{};
    bool useReserved{false};
public:
    void setNfsValidator(AddressValidator av) {
        addressValidator = av;
    };
    void setUseReserved() { useReserved = true; }
    NFSProxy(const Address &local, const Address& remote);
    bool data(Socket *sock, Byte *buf, ssize_t size) override;
    bool halfclose(Socket *sock) override;
    bool connected(Socket *sock, const Address& addr, const ServerSocket *srv) override;
    bool drain(Socket *sock) override;
    Address remoteAddr;
private:
    std::unordered_map<Socket*, Socket*> fbmap{};
    std::unordered_map<Socket*, Socket*> bfmap{};
    std::unordered_map<Socket*, NFSState> states{};
    ServerSocket proxy_srv{};
    AddressValidator addressValidator{DefaultValidator{}};
};

// do basic proxy and publish statistics to telnet port
class NFSProxyStat : public NFSProxy {
public:
    NFSProxyStat(const Address &local, const Address& remote, const Address  &telnet);
    bool connected(Socket *sock, const Address& addr, const ServerSocket *srv) override;
    bool halfclose(Socket *sock) override;
private:
    ServerSocket telnet_srv{};
    std::unordered_set<Socket*> set{};
};

// proxy mount requests
class NFSProxyMount : public NFSProxyStat {
public:
    void setMountValidator(AddressValidator av) {
        addressValidator = av;
    };
    NFSProxyMount(const Address &local, const Address& remote,
                  const Address &telnet,
                  const Address &mountlocal, const Address &mountremote);
    bool data(Socket *sock, Byte *buf, ssize_t size) override;
    bool connected(Socket *sock, const Address& addr, const ServerSocket *srv) override;
    bool halfclose(Socket *sock) override;
private:
    AddressValidator addressValidator{DefaultValidator{}};
    std::unordered_map<Socket*, Socket*> fbmap{};
    std::unordered_map<Socket*, Socket*> bfmap{};
    ServerSocket proxy_srv{};
    Address remoteAddr;
};

}

#endif
