#include "Ini.h"
#include <cctype>
#include <iostream>
#include <cstdio>
using namespace std;


// lexicons:
// SYMBOL => alpha or numerics or _ or .
// NEWLINE
// ASSIGN
// LBRACE
// RBRACE

// terms: $ = eof, ^ = nothing
//     ini = expr exprlist $ | grouplist $
//     exprlist = expr exprlist | ^
//     grouplist = group grouplist | ^
//     group = section exprlist
//     section = LBRACE SYMBOL RBRACE NEWLINE
//     expr = SYMBOL ASSIGN SYMBOL NEWLINE

namespace ini {

ostream& operator<<(ostream& stream, const Parser& parser)
{
    for (const auto &i : parser.hash) {
        stream << i.first << "=" << i.second << endl;
    }
    return stream;
}

ostream& operator<<(ostream& stream, Term term)
{
    switch (term) {
    case Term::SYMBOL:
        return stream << "symbol";
    case Term::NEWLINE:
        return stream << "newline";
    case Term::ASSIGN:
        return stream << "assign";
    case Term::LBRACE:
        return stream << "lbrace";
    case Term::RBRACE:
        return stream << "rbrace";
    case Term::END:
        return stream << "eof";
    case Term::INI:
        return stream << "ini";
    case Term::EXPRLIST:
        return stream << "exprlist";
    case Term::GROUPLIST:
        return stream << "grouplist";
    case Term::GROUP:
        return stream << "group";
    case Term::SECTION:
        return stream << "section";
    case Term::EXPR:
        return stream << "expr";
    }
}

void Lexer::complete_symbol()
{
    if (buffer.size() > 0) {
        _symbol(Term::SYMBOL, buffer);
        buffer.clear();
    }
}

void Lexer::end()
{
    update(EOF);
}

void Lexer::update(int c)
{
    if (state == State::NORMAL) {
        if (isalnum(c) || c == '.' || c == '_' || c == ':') {
            buffer += (char)c;
        } else if (c == '\n') {
            complete_symbol();
            _symbol(Term::NEWLINE, buffer);
        } else if (c == '=') {
            complete_symbol();
            _symbol(Term::ASSIGN, buffer);
        } else if (c == '[') {
            complete_symbol();
            _symbol(Term::LBRACE, buffer);
        } else if (c == ']') {
            complete_symbol();
            _symbol(Term::RBRACE, buffer);
        } else if (c == '#') {
            complete_symbol();
            state = State::COMMENT;
        } else if (c == EOF) {
            complete_symbol();
            _symbol(Term::END, buffer);
        } else if (c == '"') {
            complete_symbol();
            state = State::QUOTE;
        }
    } else if (state == State::QUOTE) {
        if (c == '"') {
            complete_symbol();
            state = State::NORMAL;
        } else if (c == EOF) {
            throw Parser::SyntaxError{};
        } else {
            buffer += (char)c;
        }
    } else { // comment
        if (c == '\n') {
            state = State::NORMAL;
        } else if (c == EOF) {
            _symbol(Term::END, buffer);
        }
    }
}

void Lexer::_symbol(Term lexicon, const std::string& value)
{
    if (!(lastlex == Term::NEWLINE && lexicon == Term::NEWLINE)) {
        symbol(lexicon, value);
    }
    lastlex = lexicon;
}

void
Parser::reset()
{
    Lexer::reset();
    inside_braces = false;
    atleftside = true;
    leftval.clear();
    rightval.clear();
    secval.clear();
    while (!tstack.empty()) { tstack.pop(); }
    tstack.push(Term::INI);
}

void
Lexer::reset()
{
    state =  State::NORMAL;
    lastlex = Term::NEWLINE;
}

void
Parser::symbol(Term lexicon, const string& value)
{
    //if (lexicon == Term::SYMBOL) {
        //cout << lexicon << ":" << value << endl;
    //} else {
        //cout << lexicon << endl;
    //}

    match(lexicon, value);
    if (lexicon == Term::END) {
        if (tstack.size() != 0) {
            throw SyntaxError{};
        }
    }
}

void
Parser::match(Term lexicon, const std::string &value)
{
    Term top = tstack.top();
    tstack.pop();
    switch (top) {
    case Term::INI:
        if (lexicon == Term::SYMBOL) {
            tstack.push(Term::END);
            tstack.push(Term::EXPRLIST);
            tstack.push(Term::EXPR);
        } else {
            tstack.push(Term::END);
            tstack.push(Term::GROUPLIST);
        }
        return match(lexicon, value);
    case Term::EXPRLIST:
        if (lexicon == Term::SYMBOL) {
            tstack.push(Term::EXPRLIST);
            tstack.push(Term::EXPR);
        }
        return match(lexicon, value);
    case Term::GROUPLIST:
        if (lexicon == Term::LBRACE) {
            tstack.push(Term::GROUPLIST);
            tstack.push(Term::GROUP);
        }
        return match(lexicon, value);
    case Term::GROUP:
        tstack.push(Term::EXPRLIST);
        tstack.push(Term::SECTION);
        return match(lexicon, value);
    case Term::SECTION:
        tstack.push(Term::NEWLINE);
        tstack.push(Term::RBRACE);
        tstack.push(Term::SYMBOL);
        tstack.push(Term::LBRACE);
        return match(lexicon, value);
    case Term::EXPR:
        tstack.push(Term::NEWLINE);
        tstack.push(Term::SYMBOL);
        tstack.push(Term::ASSIGN);
        tstack.push(Term::SYMBOL);
        return match(lexicon, value);
    case Term::SYMBOL:
        if (inside_braces) { // section header
            secval = value;
        } else if (atleftside) {
            leftval = value;
        } else { // complete assignment
            rightval = value;
            if (secval.size() == 0) {
                hash[leftval] = rightval;
            } else {
                hash[secval + "." + leftval] = rightval;
            }
            atleftside = true;
        }
        return;
    case Term::NEWLINE:
    case Term::ASSIGN:
    case Term::LBRACE:
    case Term::RBRACE:
    case Term::END:
        if (lexicon == Term::LBRACE) {
            inside_braces = true;
        } else if (lexicon == Term::RBRACE) {
            inside_braces = false;
        } else if (lexicon == Term::ASSIGN) {
            atleftside = false;
        }
        if (lexicon != top) {
            throw SyntaxError{};
        } else {
            return;
        }
        break;
    }
}

void
Lexer::update(const char *str)
{
    while (*str) {
        update((int)(*str));
        str++;
    }
}

void
Lexer::update(const string& str)
{
    for (char c : str) {
        update((int)c);
    }
}

const char *
Parser::get(const string& key) const
{
    try {
        const string& v = hash.at(key);
        return v.c_str();
    } catch (out_of_range) {
        return nullptr;
    }
}

} // end namespace ini
